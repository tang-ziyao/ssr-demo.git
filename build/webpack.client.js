const path = require('path')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const merge = require('webpack-merge')
const baseConfig = require('./webpack.base')
const VueClientPlugin = require('vue-server-renderer/client-plugin')

const isDev = process.env.NODE_ENV === 'development'

const defaultPluins = [
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: isDev ? '"development"' : '"production"'
    }
  }),
  new HTMLPlugin({
    template: path.join(__dirname, 'template.html')
  }),
  new VueClientPlugin()
]

const devServer = {
  port: 7999,
  host: '0.0.0.0',
  overlay: {
    errors: true
  },
  headers: { 'Access-Control-Allow-Origin': '*' },
  historyApiFallback: {
    index: '/public/index.html'
  },
  proxy: {
    '/api': 'http://127.0.0.1:3332',
    '/user': 'http://127.0.0.1:3332'
  },
  hot: true
}

let config

if (isDev) {
  config = merge(baseConfig, {
    target: 'web',
    entry: path.join(__dirname, '../src/clientEntry.js'),
    output: {
      filename: 'bundle.[hash:8].js',
      path: path.join(__dirname, '../public'),
      publicPath: 'http://127.0.0.1:7999/public/'
    },
    devtool: '#cheap-module-eval-source-map',
    module: {
      rules: [
        {
          test: /\.(sc|sa|c)ss/,
          use: [
            'vue-style-loader',
            'css-loader',
            'sass-loader',
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true
              }
            }
          ]
        }
      ]
    },
    devServer,
    plugins: defaultPluins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ])
  })
}

module.exports = config
