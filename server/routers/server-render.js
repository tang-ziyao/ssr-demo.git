const ejs = require('ejs')

module.exports = async (ctx, renderer, template) => {
  ctx.headers['Content-Type'] = 'text/html'
  ctx.flag = 'ctx'
  // const context = { url: ctx.path, user: ctx.session.user }
  const context = { url: ctx.path, flag: 'context' }
  console.log('flag 2')
  try {
    const appString = await renderer.renderToString(context)
    if (context.router.currentRoute.fullPath !== ctx.path) {
      console.log('flag 7')
      return ctx.redirect(context.router.currentRoute.fullPath)
    }

    // const {
    //   title
    // } = context.meta.inject()

    const html = ejs.render(template, {
      appString,
      style: context.renderStyles(),
      scripts: context.renderScripts()
    })

    ctx.body = html
    console.log('flag 6')
  } catch (err) {
    console.log('render error', err)
    throw err
  }
}
