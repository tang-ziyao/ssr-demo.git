import createApp from './createApp'

export default context => {
  return new Promise((resolve, reject) => {
    const { app, router } = createApp()

    console.log(context.flag)
    router.push(context.url)
    console.log('flag 1')
    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents()
      if (!matchedComponents.length) {
        return reject(new Error('no component matched'))
      }
      context.router = router
      resolve(app)
    })
  })
}
