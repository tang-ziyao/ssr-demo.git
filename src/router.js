import Router from 'vue-router'

const routes = [
  {
    path: '/',
    redirect: '/index'
  }, {
    path: '/index',
    component: () => import('./views/home/home'),
    redirect: '/homeContent',
    children: [
      {
        path: '/homeContent',
        name: 'homeContent',
        component: () => import('./views/home/content')
      }
    ]
  }, {
    path: '/center',
    component: () => import('./views/center/center'),
    redirect: '/centerContent',
    children: [
      {
        path: '/centerContent',
        name: 'centerContent',
        component: () => import('./views/center/content')
      }
    ]
  }
]

export default () => {
  return new Router({
    routes,
    mode: 'history',
    fallback: true
  })
}
